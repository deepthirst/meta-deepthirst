DEPENDS += "${@bb.utils.contains('ARCH', 'ppc64p9le', 'elfutils-native', '', d)}"
DEPENDS += "openssl-native util-linux-native"

COMPATIBLE_MACHINE = "qemuarm|qemuarm64|qemux86|qemuppc|qemumips|qemumips64|qemux86-64|power9"
KMACHINE ?= "power9"
KBRANCH ?= "b963ad9e15380226136c53dafdf59f71a05247a3"
KTAG ?= "v4.15.9-openpower1"
LINUX_VERSION ?= "4.15.9"

SRC_URI = " \
           git://${TOPDIR}/workspace/talos-op-linux;protocol=file;tag=${KTAG};nobranch=1 \
           file://defconfig \
           file://0001-drm-ast-Add-option-to-initialize-palette-on-driver-l.patch \
           file://0002-Force-ASpeed-RAMDAC-palette-reset.patch \
           file://0003-mtd-powernv_flash-Enable-partition-support.patch \
"
SRC_URI[md5sum] = "c417698d94c6481e85efbe7fddeb5e4a"
PV = "${LINUX_VERSION}-openpower1"
S = "${WORKDIR}/git"

inherit kernel kernel-yocto

LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"
LICENSE="GPLv2"

EXTRA_OEMAKE += " \
    KCFLAGS="-Wno-attribute-alias -Wno-stringop-truncation -Wno-implicit-function-declaration -Wno-stringop-overflow -Wno-packed-not-aligned -Wno-unused-variable -Wno-address-of-packed-member" \
"