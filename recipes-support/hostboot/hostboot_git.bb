LICENSE = "Unknown"
LIC_FILES_CHKSUM = "file://LICENSE;md5=34400b68072d710fecd0a2940a0d1658 \
                    file://LICENSE_PROLOG;md5=d8e5f403c98fd80dcea90b9cc8cd083c \
                    file://src/build/tools/copyright-check.sh;md5=fd915029c0e9d3675e90b69fb9f08245 \
                    file://src/usr/xz/LICENSE_PROLOG;md5=4b0d2ed9ffc090e02813ebff43bdfd9d \
                    file://src/include/usr/xz/LICENSE_PROLOG;md5=4b0d2ed9ffc090e02813ebff43bdfd9d \
                    file://src/include/securerom/contrib/LICENSE_PROLOG;md5=1386284981e1404b801bf655ac9c5f91 \
                    file://src/securerom/contrib/LICENSE_PROLOG;md5=1386284981e1404b801bf655ac9c5f91"

SRC_URI = " \
    git://git.raptorcs.com/git/talos-hostboot/;protocol=https;branch=07-25-2019; \
    file://0001-uprev-calls-to-bfd-api.patch \
    file://talos.config \
"

PV = "1.0+git${SRCPV}"
#SRCREV = "884b60b16009061ab84db88f918902a8c8098a4b"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"


DEPENDS += "binutils-native talos-gcc-native talos-binutils-native perl-native bison-native flex-native"
inherit native

LDFLAGS=''

EXTRA_OEMAKE += " \
    SKIP_BINARY_FILES=1 \
    JAILCMD='' \
    PERL_USE_UNSAFE_INC=1 \
    CONFIG_FILE=${WORKDIR}/talos.config \
    CROSS_PREFIX='${RECIPE_SYSROOT_NATIVE}/usr/bin/powerpc64le-talos-linux-gnu-' \
    HOST_PREFIX='' \
    HOST_BINUTILS_DIR='${RECIPE_SYSROOT_NATIVE}/usr' \
    PROJECT_NAME=Hostboot \
    PROJECT_ROOT=${WORKDIR}/git \
"

do_compile () {
	oe_runmake
}

do_install () {
        # NOTE: unable to determine what to put here - there is a Makefile but no
        # target named "install", so you will need to define this yourself
        :
}
