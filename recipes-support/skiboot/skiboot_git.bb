LIC_FILES_CHKSUM = "file://ccan/check_type/LICENSE;md5=c17af43b05840255a6fedc5eda9d56cc"
LICENSE = "Unknown"

BRANCH="04-16-2019"
SRC_URI = " \
    git://scm.raptorcs.com/scm/git/talos-skiboot/;protocol=https;branch=${BRANCH} \
    file://0001-disable-gcc-warnings.patch \
"

PV = "1.0+git${SRCPV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

EXTRA_OEMAKE += " \
    CROSS=${TARGET_PREFIX} \
"

do_compile () {
	oe_runmake
}

do_install () {
        # NOTE: unable to determine what to put here - there is a Makefile but no
        # target named "install", so you will need to define this yourself
        :
}
