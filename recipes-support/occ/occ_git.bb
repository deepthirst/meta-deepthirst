LICENSE = "Unknown"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d2794c0df5b907fdace235a619d80314"
DEPENDS = "ppe42-gcc-native ppe42-binutils-native talos-gcc-native talos-binutils-native"

BRANCH = "06-18-2018"
SRC_URI = " \
    git://git.raptorcs.com/git/talos-occ/;protocol=https;branch=${BRANCH}; \
"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

# NOTE: the following library dependencies are unknown, ignoring: bfd pthread dl iberty z
#       (this is based on recipes that have previously been built and packaged)

# NOTE: this is a Makefile-only piece of software, so we cannot generate much of the
# recipe automatically - you will need to examine the Makefile yourself and ensure
# that the appropriate arguments are passed in.

TARGET_LDFLAGS=""
EXTRA_OEMAKE += " \
    CROSS_PREFIX=${RECIPE_SYSROOT_NATIVE}/usr/bin/powerpc64le-talos-linux-gnu- \
    PPE_TOOL_PATH=${RECIPE_SYSROOT_NATIVE}/usr \
    OCC_OP_BUILD=1 \
"
do_compile () {
    cd ${S}/src
    oe_runmake all
}

do_install () {
        # NOTE: unable to determine what to put here - there is a Makefile but no
        # target named "install", so you will need to define this yourself
        :
}

BBCLASSEXTEND = "native nativesdk"