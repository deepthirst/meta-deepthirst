LIC_FILES_CHKSUM = "file://COPYING;md5=393a5ca445f6965873eca0259a17f833"
LICENSE = "Unknown"

DEPENDS = "flex-native bison-native zlib-native gnu-config-native autoconf-native udev"
inherit autotools gettext multilib_header texinfo

TAG="v1.7.1"
SRC_URI = " \
    git://scm.raptorcs.com/scm/git/talos-petitboot/;protocol=https;tag=${TAG};no-branch=1 \
"

PV = "1.0+git${SRCPV}"

S = "${WORKDIR}/git"

EXTRA_OECONF += " \
    --with-twin-x11=no \
    --with-twin-fbdev=no \
"

do_configure () {
    cd ${S}
    autoreconf -f -i
    oe_runconf
}

do_compile () {
    cd ${S}
    oe_runmake
}

do_install () {
        # NOTE: unable to determine what to put here - there is a Makefile but no
        # target named "install", so you will need to define this yourself
        :
}
