LICENSE = "Unknown"
LIC_FILES_CHKSUM = "file://LICENSE_PROLOG;md5=d8e5f403c98fd80dcea90b9cc8cd083c"

BRANCH = "05-14-2018"
SRC_URI = " \
    git://git.raptorcs.com/git/talos-hcode/;protocol=https;branch=${BRANCH}; \
"

inherit native
DEPENDS = "ppe42-gcc-native ppe42-binutils-native talos-gcc-native talos-binutils-native"

PV = "1.0+git${SRCPV}"
SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"

LDFLAGS=''
EXTRA_OEMAKE += " \
    PPE_TOOL_PATH='${RECIPE_SYSROOT_NATIVE}/usr' \
    __EKB_PREFIX='' \
    HOST_PREFIX=''\
    TARGET_PREFIX=''\
    OPENPOWER_BUILD=1\
    LD_LIBRARY_PATH=${RECIPE_SYSROOT_NATIVE}/usr/lib \
    CONFIG_INCLUDE_IONV=0 \
    CROSS_COMPILER_PATH='${RECIPE_SYSROOT_NATIVE}/usr' \
    PPE_PREFIX=${RECIPE_SYSROOT_NATIVE}/usr/bin/powerpc-eabi- \
    PPE_BINUTILS_PREFIX=${RECIPE_SYSROOT_NATIVE}/usr/bin/powerpc-eabi- \
    RINGFILEPATH=${S}/rings/ \
"

do_compile () {
    oe_runmake
}

do_install () {
        # NOTE: unable to determine what to put here - there is a Makefile but no
        # target named "install", so you will need to define this yourself
        :
}