LICENSE = "Unknown"
LIC_FILES_CHKSUM = "file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552"
DEPENDS = "flex-native bison-native zlib-native gnu-config-native autoconf-native"
inherit autotools gettext multilib_header texinfo native

BRANCH = "binutils-2_24-ppe42"
SRC_URI = " \
    git://git.raptorcs.com/git/ppe42-binutils/;protocol=https;branch=${BRANCH}; \
"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

# NOTE: the following library dependencies are unknown, ignoring: bfd pthread dl iberty z
#       (this is based on recipes that have previously been built and packaged)

# NOTE: this is a Makefile-only piece of software, so we cannot generate much of the
# recipe automatically - you will need to examine the Makefile yourself and ensure
# that the appropriate arguments are passed in.

PROVIDES = "${PN}"

FILES_${PN} = "\
    ${bindir}/* \
"

EXTRA_OEMAKE += " \
    LDFLAGS=-static \
    CFLAGS=-Wno-error \
"

EXTRA_OECONF = "--target=powerpc-eabi \
                --enable-shared \
                --enable-64-bit-bfd"

do_configure () {
    cd ${S}
    oe_runconf
}

do_compile () {
    cd ${S}
    oe_runmake
}

do_install () {
    cd ${S}
    oe_runmake 'DESTDIR=${D}' install
}

BBCLASSEXTEND = "native nativesdk"