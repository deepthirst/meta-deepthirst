# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   COPYING.RUNTIME
#   libstdc++-v3/doc/html/manual/license.html
#   zlib/contrib/dotzlib/LICENSE_1_0.txt
#   libffi/LICENSE
#   libjava/LIBGCJ_LICENSE
#   libjava/libltdl/COPYING.LIB
#   libjava/classpath/LICENSE
#   libjava/classpath/COPYING
#   libjava/classpath/tools/classes/gnu/classpath/tools/taglets/CopyrightTaglet.class
#   libjava/classpath/tools/gnu/classpath/tools/taglets/CopyrightTaglet.java
#   libsanitizer/LICENSE.TXT
#   libgo/LICENSE
#   gcc/go/gofrontend/LICENSE
#   gcc/ada/doc/share/gnu_free_documentation_license.rst
#
# NOTE: multiple licenses have been detected; they have been separated with &
# in the LICENSE value for now since it is a reasonable assumption that all
# of the licenses apply. If instead there is a choice between the multiple
# licenses then you should change the value to separate the licenses with |
# instead of &. If there is any doubt, check the accompanying documentation
# to determine which situation is applicable.
LICENSE = "LGPLv3 & LGPLv2.1 & Unknown & GPLv2 & GPLv3"
LIC_FILES_CHKSUM = "file://COPYING3.LIB;md5=6a6a8e020838b23406c81b19c1d46df6 \
                    file://COPYING.LIB;md5=2d5025d4aa3495befef8f17206a5b0a1 \
                    file://COPYING.RUNTIME;md5=fe60d87048567d4fe8c8a0ed2448bcc8 \
                    file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552 \
                    file://COPYING3;md5=d32239bcb673463ab874e80d47fae504 \
                    file://libquadmath/COPYING.LIB;md5=a916467b91076e631dd8edb7424769c7 \
                    file://libiberty/COPYING.LIB;md5=a916467b91076e631dd8edb7424769c7 \
                    file://libstdc++-v3/doc/html/manual/license.html;md5=f2cbe7a16c1459eb9d661c4f59148c85 \
                    file://zlib/contrib/dotzlib/LICENSE_1_0.txt;md5=81543b22c36f10d20ac9712f8d80ef8d \
                    file://libffi/LICENSE;md5=3610bb17683a0089ed64055416b2ae1b \
                    file://libjava/COPYING;md5=94d55d512a9ba36caa9b7df079bae19f \
                    file://libjava/LIBGCJ_LICENSE;md5=e6044391ca5876bd430bc51e9e144cf6 \
                    file://libjava/libltdl/COPYING.LIB;md5=e3eda01d9815f8d24aae2dbd89b68b06 \
                    file://libjava/classpath/LICENSE;md5=92acc79f1f429143f4624d07b253702a \
                    file://libjava/classpath/COPYING;md5=af0004801732bc4b20d90f351cf80510 \
                    file://libjava/classpath/tools/classes/gnu/classpath/tools/taglets/CopyrightTaglet.class;md5=c32437f2935986fb54fd099a22f3aac0 \
                    file://libjava/classpath/tools/gnu/classpath/tools/taglets/CopyrightTaglet.java;md5=26986f388a17986ff88946818131664c \
                    file://libsanitizer/LICENSE.TXT;md5=0249c37748936faf5b1efd5789587909 \
                    file://include/COPYING;md5=59530bdf33659b29e73d4adb9f9f6552 \
                    file://include/COPYING3;md5=d32239bcb673463ab874e80d47fae504 \
                    file://libgo/LICENSE;md5=5d4950ecb7b26d2c5e4e7b4e0dd74707 \
                    file://gcc/COPYING3.LIB;md5=6a6a8e020838b23406c81b19c1d46df6 \
                    file://gcc/COPYING.LIB;md5=a916467b91076e631dd8edb7424769c7 \
                    file://gcc/COPYING;md5=59530bdf33659b29e73d4adb9f9f6552 \
                    file://gcc/COPYING3;md5=d32239bcb673463ab874e80d47fae504 \
                    file://gcc/go/gofrontend/LICENSE;md5=5d4950ecb7b26d2c5e4e7b4e0dd74707 \
                    file://gcc/ada/doc/share/gnu_free_documentation_license.rst;md5=9f43695687d6ab9372dfe236a9829279"

SRC_URI = "https://ftp.gnu.org/gnu/gcc/gcc-${PV}/gcc-${PV}.tar.gz"
SRC_URI[md5sum] = "3124e1563958a24a64210236852f7283"
SRC_URI[sha1sum] = "b22ff1effc370c55e389438561f44c88f7cd047d"
SRC_URI[sha256sum] = "4715f02413f8a91d02d967521c084990c99ce1a671b8a450a80fbd4245f4b728"
SRC_URI[sha384sum] = "14894df092fd4ee12912642e36ceec181c45063e73bab0a474c3160b1068ad5156d01e22cf01e30f81ff0bb7299db28a"
SRC_URI[sha512sum] = "04a9d461568eea1c7f023690a96c40d8825e47812b478fc27161023cd8549a3fc56cfad6de5d0a0591f44c468ec861b438a2af2e20f4e3dd9952a3d242713178"

S = "${WORKDIR}/gcc-${PV}"

# NOTE: the following prog dependencies are unknown, ignoring: yacc runtest byacc lex gm4 gnum4 expect
DEPENDS = "flex-native bison-native zlib-native gnu-config-native autoconf-native gmp-native mpfr-native libmpc-native talos-binutils-native"

# NOTE: if this software is not capable of being built in a separate build directory
# from the source, you should replace autotools with autotools-brokensep in the
# inherit line
inherit autotools gettext multilib_header texinfo native

# Specify any options you want to pass to the configure script using EXTRA_OECONF:

EXTRA_OECONF = " \
    --enable-static \
    --target=powerpc64le-talos-linux-gnu \
    --disable-__cxa_atexit \
    --with-gnu-ld \
    --with-as=${RECIPE_SYSROOT_NATIVE}/usr/bin/powerpc64le-talos-linux-gnu-as \
    --disable-libssp \
    --disable-multilib \
    --with-bugurl=http://bugs.buildroot.net/ \
    --disable-libquadmath \
    --enable-tls \
    --disable-libmudflap \
    --enable-threads \
    --without-isl \
    --without-cloog \
    --disable-decimal-float \
    --with-cpu=power8 \
    --with-long-double-128 \
    --enable-languages=c,c++ \
    --enable-shared \
    --disable-libgomp \
    --enable-targets=powerpc64-linux \
    --with-gmp=${RECIPE_SYSROOT_NATIVE}/usr \
    --with-mpfr=${RECIPE_SYSROOT_NATIVE}/usr "

do_configure () {
    oe_runconf
}

do_compile () {
    oe_runmake all-gcc
}

do_install () {
    oe_runmake 'DESTDIR=${D}' install-gcc
}

BBCLASSEXTEND = "native nativesdk"