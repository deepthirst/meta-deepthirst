LICENSE = "Unknown"
LIC_FILES_CHKSUM = "file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552"
DEPENDS = "flex-native bison-native zlib-native gnu-config-native autoconf-native gmp-native mpfr-native libmpc-native"
inherit autotools gettext multilib_header texinfo native

BRANCH = "gcc-4_9_2-ppe42"
SRC_URI = " \
    git://git.raptorcs.com/git/ppe42-gcc/;protocol=https;branch=${BRANCH}; \
    file://0001-2016-02-19-Jakub-Jelinek-jakub-redhat.com.patch \
"

PV = "1.0+git${SRCPV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

PROVIDES = "${PN}"

FILES_${PN} = "\
    ${bindir}/* \
"

EXTRA_OECONF = "--target=powerpc-eabi \
                --without-headers \
                --with-newlib \
                --with-gnu-as \
                --with-gnu-ld \
                --with-gmp=${RECIPE_SYSROOT_NATIVE}/usr \
                --with-mpfr=${RECIPE_SYSROOT_NATIVE}/usr "

do_configure () {
    cd ${S}
    oe_runconf
}

do_compile () {
    cd ${S}
    oe_runmake all-gcc
}

do_install () {
    cd ${S}
    oe_runmake 'DESTDIR=${D}' install-gcc
}

BBCLASSEXTEND = "native nativesdk"