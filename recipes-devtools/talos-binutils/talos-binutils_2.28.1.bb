# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   zlib/contrib/dotzlib/LICENSE_1_0.txt
#
# NOTE: multiple licenses have been detected; they have been separated with &
# in the LICENSE value for now since it is a reasonable assumption that all
# of the licenses apply. If instead there is a choice between the multiple
# licenses then you should change the value to separate the licenses with |
# instead of &. If there is any doubt, check the accompanying documentation
# to determine which situation is applicable.
LICENSE = "LGPLv3 & LGPLv2 & GPLv2 & GPLv3 & LGPLv2.1 & Unknown"
LIC_FILES_CHKSUM = "file://COPYING3.LIB;md5=6a6a8e020838b23406c81b19c1d46df6 \
                    file://COPYING.LIB;md5=9f604d8a4f8e74f4f5140845a21b6674 \
                    file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552 \
                    file://COPYING3;md5=d32239bcb673463ab874e80d47fae504 \
                    file://libiberty/COPYING.LIB;md5=a916467b91076e631dd8edb7424769c7 \
                    file://bfd/COPYING;md5=d32239bcb673463ab874e80d47fae504 \
                    file://gas/COPYING;md5=d32239bcb673463ab874e80d47fae504 \
                    file://zlib/contrib/dotzlib/LICENSE_1_0.txt;md5=81543b22c36f10d20ac9712f8d80ef8d \
                    file://include/COPYING;md5=59530bdf33659b29e73d4adb9f9f6552 \
                    file://include/COPYING3;md5=d32239bcb673463ab874e80d47fae504"

SRC_URI = "https://ftp.gnu.org/gnu/binutils/binutils-${PV}.tar.gz"
SRC_URI[md5sum] = "403206b93ef94bf2c88f218e84d59375"
SRC_URI[sha1sum] = "806edd56bf3c933bd17c542c4fdfde11e4249d2a"
SRC_URI[sha256sum] = "76d4a9532bf78361bbced09a3df0a60d0d981a62b7773b380fc6933a734202c0"
SRC_URI[sha384sum] = "ce12d3db3f846fe5b24e668f0b28a1f1d08eedb6b9cbedc9a3c9c044dfd6c75e72ef5ca3ffd52d72d13a10ddf1eeeeac"
SRC_URI[sha512sum] = "c4437e2e001ffed01cb5f39c0cbc4c0f3a1594ab3274f7f83d8a3e57197e4ac259f8a578d0d40a7bf6944792b301ce3e6fdddf798ea72128f6b005d175fe5437"

S = "${WORKDIR}/binutils-${PV}"

DEPENDS = "flex-native bison-native zlib-native gnu-config-native autoconf-native"
inherit autotools gettext multilib_header texinfo native

# Specify any options you want to pass to the configure script using EXTRA_OECONF:
EXTRA_OECONF = "--enable-shared \
    --disable-static \
    --disable-gtk-doc \
    --disable-gtk-doc-html \
    --disable-doc \
    --disable-docs \
    --disable-documentation \
    --disable-debug \
    --with-xmlto=no \
    --with-fop=no \
    --disable-dependency-tracking \
    --disable-multilib \
    --disable-werror \
    --target=powerpc64le-talos-linux-gnu \
    --disable-shared \
    --enable-static \
    --enable-poison-system-directories \
    --disable-sim \
    --disable-gdb \
    --enable-targets=powerpc64-linux"

do_configure () {
    oe_runconf
}

do_compile () {
    oe_runmake
}

do_install () {
    oe_runmake 'DESTDIR=${D}' install
}

BBCLASSEXTEND = "native nativesdk"